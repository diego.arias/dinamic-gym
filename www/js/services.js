angular.module('dbservices', [])
.service('DBService', function($q) {
  return this
})
.service('ClienteService',function($q){
  var _dniCliente = '';
  var _cliente = {
      _id : '',
      nombre: '',
      apellido: '',
      dni:'',
      actividades : [

      ],
      pagos:[]

  };
  var _clientes = [];
  var _actividad = {};
  var _asistencias = {};
  var _tipoReporte = {};

  this.cliente = _cliente;
  this.dniCliente = _dniCliente;
  this.actividad = _actividad;
  this.asistencias = _asistencias;
  this.tipoReporte = _tipoReporte;
  this.clientes = _clientes;

  var db ;

  var items;
  var self = this;

  this.initDB = function() {
    db = new PouchDB('clientes');
    return db;
  };

  this.getDB = function() {
    if(db== null)
    self.initDB();
      return $q.when(
          db.allDocs({
            include_docs: true,
            deleted: false
          }))
        .then(function(docs) {
          items = docs.rows.map(function(row) {
            return row.doc;
          });

          // Listen for changes on the database.
          db.changes({
              live: true,
              since: 'now',
              include_docs: true
            })
            .on('change', function(change) {
              self.onDatabaseChange(change)
            });
          return items;
        });


  };
  this.removeItem = function(item){
    if(db== null)
    self.initDB();
    db.remove(item);

  }

  this.onDatabaseChange = function(change) {
    var index = self.findIndex(items, change.id);
    var item = items[index];

    items.splice(index, 0, change.doc) // insert
  }

  this.findIndex = function(array, id) {
    var low = 0,
      high = array.length,
      mid;
    while (low < high) {
      mid = (low + high) >>> 1;
      array[mid]._id < id ? low = mid + 1 : high = mid
    }
    return low;
  }

  this.storeData = function(data) {
    return $q.when(db.put(data, function callback(err, result) {
      self.onDatabaseChange(data);
    }));
  };




})
.service('ActividadService',function($q){


var items;
  var dbActividades;
  var self = this;
  this.initDB = function() {
    dbActividades = new PouchDB('actividades');
    return dbActividades;
  };

  this.getDB = function() {
    if(dbActividades==null)
      self.initDB();
      return $q.when(
          dbActividades.allDocs({
            include_docs: true
          }))
        .then(function(docs) {
          items = docs.rows.map(function(row) {
            return row.doc;
          });

          // Listen for changes on the database.
          dbActividades.changes({
              live: true,
              since: 'now',
              include_docs: true
            })
            .on('change', function(change) {
              self.onDatabaseChange(change)
            });
          return items;
        });


  };

  this.onDatabaseChange = function(change) {
    var index = self.findIndex(items, change.id);
    var item = items[index];

    items.splice(index, 0, change.doc) // insert

  }

  this.findIndex = function(array, id) {
    var low = 0,
      high = array.length,
      mid;
    while (low < high) {
      mid = (low + high) >>> 1;
      array[mid]._id < id ? low = mid + 1 : high = mid
    }
    return low;
  }


   this.storeData = function(data) {
    return self.getDB(dbActividades.put(data, function callback(err, result) {

    }));
  };

})
