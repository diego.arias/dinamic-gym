angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {})
.controller('PlaylistsCtrl', function($scope, ClienteService) {})
.controller('LoginCtrl', function($scope, $stateParams,$ionicPopup,ClienteService,$state,base64) {

  $scope.dni = {dni:'',clave:''};
  $scope.actividades ;
  $scope.finger=false;

  $scope.enterDNI = function(){
      $scope.finger=false;
      $state.go('app.asistencia');
  }
  $scope.loginFinger = function(){
      $scope.finger=false;
      $state.go('app.finger');
  }

  $scope.enterFinger = function(){
    $scope.finger=true;
    FingerprintAuth.show({
              clientId: "dinamic",
              clientSecret: "test"
          }, $scope.successCallback, $scope.errorCallback);
  }
  /**
 * @return {withFingerprint:base64EncodedString, withPassword:boolean}
 */
    $scope.successCallback = function(result) {
      $scope.ingresar();
    }

    $scope.errorCallback = function(error) {
      $scope.ingresar();
    }
  $scope.ingresar = function(){
    ClienteService.getDB().then(function(result){

      angular.forEach(result,function(element) {

          if(element._id == $scope.dni.dni){
            if(!$scope.finger &&  element.password != $scope.dni.clave){

              var alertPopup = $ionicPopup.alert({
                title: 'Error',
                template: 'Los datos ingresdos no son válidos, ingrese nuevamente.'
              });

                alertPopup.then(function(res) {
                $state.go("app.playlists");
              });

            }else{
                  $scope.cliente = element;
                  ClienteService.cliente = element;
                    var deudor = false;
                    if(element.pagos == null ||element.pagos.length<=0) {
                      deudor = true;
                    }else{
                      for(var i = 0 ; i<element.pagos.length;i++){
                        if(isNaN(element.pagos[i].actividad.costo)){
                          deudor =  true;
                        }
                      }
                  }
                  if(deudor){
                    var alertPopup = $ionicPopup.alert({
                      title: 'Cobranzas',
                      template: 'Recuerde regularizar su situacion.'
                    });

                    alertPopup.then(function(res) {

                    });
                  }

            }
        }
        }, this);


    });


    $scope.showActividades = function(cliente, actividad){
      ClienteService.cliente = cliente;
      ClienteService.actividad = actividad;
      $scope.dni.dni = '';
      $scope.cliente = null;
       ClienteService.asistencias=[];
      angular.forEach(cliente.pagos,function(value,key){
      if(value.actividad._id == actividad._id){

        ClienteService.asistencias = ClienteService.asistencias.concat(value.asistencias);
      ClienteService.frecuencia=value.actividad.frecuencia;
      }

    });
    //ClienteService.asistencias.push($scope.tmp);
      $state.go('app.listaAsistencia');
    }


  }




})
.controller('ClientesCtrl', function($scope, $stateParams,DBService,$state,ClienteService,base64,$ionicPopup,
   $ionicHistory,$interval, ActividadService,$ionicLoading) {
     $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
    if(ClienteService.cliente!=null){
      $scope.cliente = ClienteService.cliente;
     // $scope.allActividades = ClienteService.cliente.actividades;
    }else{
      $scope.cliente = {};

    }
  $scope.allActividades = [];

  ActividadService.getDB().then(function(result){
    $scope.allActividades = result;
  });

    ClienteService.getDB().then(function(result){
      $scope.clientes = result;
      $ionicLoading.hide();
    });
    $scope.viewDetails = function(item){
       ClienteService.cliente = item;

      $scope.cliente = item;


       $state.go('app.addclientes');

    }

    $scope.addItem = function(){
      $state.go('app.addclientes');
    }

    $scope.registrarHuella = function(){
      FingerprintAuth.show({
              clientId: "dinamic",
              clientSecret: $scope.cliente._id
          }, $scope.successCallback, $scope.errorCallback);
    }
    $scope.successCallback = function(result) {}

    $scope.errorCallback = function(error) {}

    $scope.save = function(){
      var actividadesSeleccionadas=new Promise(function(resolve,reject){
        var filtradas = $scope.allActividades.filter(function(row){
          if(row.checked) {

            return true;
          }
        });
        resolve(filtradas);
      });
      actividadesSeleccionadas.then(function(filtradas){

      $scope.cliente.actividades = filtradas;

      ClienteService.storeData($scope.cliente) ;
      ClienteService.cliente = null;
      $ionicHistory.goBack();
      });

    }
    $scope.refreshList = function(){
      ClienteService.getDB().then(function(result){
        $scope.clientes = result;
      });
    }
    $scope.esDeudor = function(item){
      if(item.pagos == null ||item.pagos.length<=0) {
        return true;
      }else{
        for(var i = 0 ; i<item.pagos.length;i++){
          if(isNaN(item.pagos[i].actividad.costo)){
            return true;
          }
        }
      }

      return false;
    }

    $scope.init = function () {


    }

    $scope.eliminarCliente = function(item){
        var confirmPopup = $ionicPopup.confirm({
          title: 'Eliminar cliente',
          template: 'Está seguro de eliminar el cliente?'
        });

        confirmPopup.then(function(res) {
          if(res) {
            ClienteService.removeItem(item);
            $scope.refreshList();
          } else {
            console.log('You are not sure');
          }
        });
    }
    $scope.verPagosCliente = function(item){
      ClienteService.cliente = item;
      $scope.cliente = item;

      $scope.pagosCliente= item.pagos;
      $state.go("app.verPagos");
    }

    $scope.ingresarPago = function(item){

      ClienteService.cliente = item;
      ClienteService.cliente.dni = item._id;

      angular.forEach(ClienteService.cliente.actividades, function(value, key) {
        value.checked = false;
      });

      $state.go("app.ingresarPago");


    }

    $scope.pagar = function(){
      $scope.clientes = [];
      var pagos = [];
      $scope.total = 0;

      angular.forEach($scope.cliente.actividades,function(value,key){

        if(value.checked){

            var pago = {
              _id : guid(),
              actividad: '',
              fecha:''
            };

            if(pago.asistencias == null) pago.asistencias = [];
            for(var i=0;i<parseInt(value.frecuencia);i++){
               var asistencia = {
                _id: guid(),
                fecha: null,
                asistio: '',
                firma: null
              };
              pago.asistencias.push(asistencia);
            }

            pago._id = $scope.cliente.dni + "_" + value._id + "_" + new Date();
            pago.actividad = value;
            pago.fecha = new Date();
            if($scope.cliente.pagos==null) $scope.cliente.pagos = [];
            $scope.cliente.pagos.push(pago);


        }

      });
      ClienteService.storeData($scope.cliente) ;
      $state.go("app.clientes");
    }
    $scope.total = 0;

    $scope.getPagoTotal = function(){
      $scope.total = 0;
      angular.forEach($scope.cliente.actividades,function(value,key){
        if(!Number.isNaN(value.costo)){
          if(value.checked && (value.costo!=null)){
            $scope.total += parseInt(value.costo);
          }
        }
      });
      return $scope.total;
    }
})
.controller('ActividadesCtrl', function($scope, $stateParams,DBService,$state,ActividadService,$ionicHistory) {

  $scope.actividad = {
    _id : '',
    nombre: '',
    frecuencia : ''
  };
 $scope.actividades = [];
  ActividadService.getDB().then(function(result){
    $scope.actividades =  result;
  });


  $scope.addItem = function(){
    $state.go('app.addactividades');
  }
  $scope.save = function(){

    ActividadService.storeData($scope.actividad).then(function(result){
      $ionicHistory.goBack();
    });
  }
   $scope.refreshList = function(){
      ActividadService.getDB().then(function(result){
        $scope.actividades =  result;
      });
    }


})
.controller('AsistenciaCtrl', function($scope, $stateParams,$ionicPopup,ClienteService,$state,$ionicHistory) {
    $scope.recuperadas = {
      cantidad : 0
    };

    $scope.cliente = ClienteService.cliente;

    $scope.actividad = ClienteService.actividad;
    $scope.asistencias = ClienteService.asistencias;
    $scope.frecuencia = ClienteService.frecuencia;
    $scope.asistencia  = {
      fecha: '',
      asistio: ''
    };

    var follow = true;
    angular.forEach($scope.asistencias,function(aValue,aKey){
      if(follow){
        if(aValue.fecha == '' || aValue.fecha==null){
          aValue.fecha = new Date();
          $scope.asistencia.fecha = new Date();
          $scope.indiceActividad = aKey;
          follow = false;
        }
      }

    });
    $scope.marcarIngreso = function(itemId){

      angular.forEach($scope.asistencias, function(value, key){
         if(value._id == itemId){
           value.firma = "realizada";
           if(value.asistio == ""){
             value.asistio = false;
             value.firma = "ausente";
           }
        }
      } );
       ClienteService.storeData($scope.cliente) ;
      $scope.asistencia  = {
        fecha: '',
        asistio: ''
      };

      $ionicHistory.goBack();
     //$state.go("app.playlists");

    }
   $scope.recuperar = function(itemId){
     $scope.asistencias = [];
     for(var i = 0 ; i < parseInt($scope.recuperadas.cantidad);i++){
      var asistencia = {
          _id: guid(),
          fecha: null,
          asistio: '',
          firma: null
        };

      $scope.asistencias.push(asistencia);

    }
    //$scope.cliente.asistencias = $scope.asistencias;
     ClienteService.storeData($scope.cliente) ;
     var alertPopup = $ionicPopup.alert({
        title: 'Recuperacion de clase',
        template: 'Se ingresarán las clases recuperadas. Por favor, ingrese nuevamente.'
      });

      alertPopup.then(function(res) {
        $state.go('app.asistencia');
      });

   }

})
.controller('ReportesCtrl', function($scope,$state,ClienteService,$filter, $ionicLoading,$ionicPopup,$ionicHistory) {
  $scope.reportes = [
    {nombre: "Reporte diario", cod:"reporteDiario"},
    {nombre: "Reporte mensual",cod:"reporteMensual"}
  ];
  $scope.tipoReporte=ClienteService.tipoReporte;
  $scope.total = 0;
  $scope.pagosDelDia = [];
  $scope.cliente;
   $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
    $scope.armarReporte=function(){
       $scope.total = 0;
       var fechaActual = new Date();
     var fechaActualFormateada = $filter('date')(fechaActual, "dd/MM/yyyy");
     ClienteService.getDB().then(function(result){
       $scope.cliente = result;
        for(var i = 0 ; i< result.length ; i++){
          if(result[i].pagos == null) result[i].pagos = [];
         for(var j = 0; j< result[i].pagos.length; j++){

              var idate = $filter('date')(result[i].pagos[j].fecha, "dd/MM/yyyy");
              var pagoDiario = {
                  cliente: {},
                  pago: {}
              }
              if($scope.tipoReporte.cod == "reporteDiario"){

                  if(idate == fechaActualFormateada){
                    pagoDiario.cliente = result[i];
                    pagoDiario.pago = result[i].pagos[j]
                    $scope.pagosDelDia.push(pagoDiario);
                    if(isNaN(result[i].pagos[j].actividad.costo)) result[i].pagos[j].actividad.costo = 0;
                    $scope.total += parseInt(result[i].pagos[j].actividad.costo );

                  }
              }else if($scope.tipoReporte.cod == "reporteMensual"){
                  var hoy = new Date();
                  var firstOfMonth = new Date(hoy.getTime() - ((((60 * 1000) * 60)*24)* hoy.getDate() ) );
                  var fechaPago = new Date(result[i].pagos[j].fecha);
                  if(fechaPago.getTime() >= firstOfMonth.getTime() ){
                    pagoDiario.cliente = result[i];
                    pagoDiario.pago = result[i].pagos[j]
                    $scope.pagosDelDia.push(pagoDiario);
                    if(isNaN(result[i].pagos[j].actividad.costo)) result[i].pagos[j].actividad.costo = 0;
                    $scope.total += parseInt( result[i].pagos[j].actividad.costo );
                  }


              }
         }
       }
        $ionicLoading.hide();
     })


    }

    $scope.armarReporte();
  $scope.verReporte = function(item){
    ClienteService.tipoReporte = item;
    $state.go("app.reporteDiario");
  }
  $scope.eliminarPago = function(item){
        var confirmPopup = $ionicPopup.confirm({
          title: 'Eliminar pago',
          template: 'Está seguro de eliminar el pago?'
        });

        confirmPopup.then(function(res) {
          if(res) {
           // ClienteService.removeItem(item);
            angular.forEach( item.cliente.pagos,function(value,key){
              if(value._id == item.pago._id){
                item.cliente.pagos.splice(key,1);
                ClienteService.storeData(item.cliente);
                $ionicHistory.goBack();
              }
            })
          } else {
            console.log('You are not sure');
          }
        });
    }

})
.controller('SettingsCtrl', function($scope, $stateParams,$ionicPopup,ClienteService,$state,$ionicHistory) {

  var frecHabitual = [2,3];

  $scope.listaCorreos = {

  };
  $scope.correo = {
    direccion: ''
  };


  $scope.cerrarAistencias = function(){

    db.allDocs({include_docs: true, descending: false}, function(err, doc) {
      //doc.rows[0].doc.pagos[3].asistencias
      angular.forEach(doc.rows,function(value,key){

      });
    });
    var alertPopup = $ionicPopup.alert({
        title: 'Proceso de cierre',
        template: 'Las asistencias fueron completadas.'
      });
      alertPopup.then(function(res) { });
  }
  $scope.borrarDatos = function(){
    var db = new PouchDB('clientes');
    var dbActividades = new PouchDB('actividades');
    db.allDocs().then(function (result) {

      return Promise.all(result.rows.map(function (row) {
        return db.remove(row.id, row.value.rev);
      }));
    });

  /*  dbActividades.allDocs().then(function (result) {
      return Promise.all(result.rows.map(function (row) {
        return dbActividades.remove(row.id, row.value.rev);
      }));
    });*/
  }

  $scope.borrarPago = function(){
     ClienteService.getDB().then(function(result){
        for(var i=0;i<result.length;i++) {
          if(result[i]._id=="24973493"){
            var tmp = result[i].pagos[0];
            result[i].pagos =  [];
            result[i].pagos.push(tmp);
             ClienteService.storeData(result[i]);
          }

        }
     });
  }
  $scope.fixdni = function(){
     var db = new PouchDB('clientes');
    for(var i = 0 ; i < jtmp.length;i++){
      jtmp[i]._rev = null;

    }
    for(var i = 0 ; i < jtmp.length;i++){
   db.put(jtmp[i],function(error,result){
       console.log(error + " " + result + " " + i);
    })
    }

  }
})
.controller('MenuCtrl', function($scope, $stateParams,DBService,$state,ClienteService) {

  $scope.goToPagos = function(){
    ClienteService.getDB().then(function(result){
      $scope.clientes = result;
      ClienteService.clientes = result;
      $state.go("app.pagos");
    });
  }
  $scope.goToClientes = function(){
    ClienteService.getDB().then(function(result){
      $scope.clientes = result;
      ClienteService.clientes = result;
      $state.go("app.clientes");
    });
  }

});

